#!/bin/bash
#
# Performs lossless image optimization on PNG files.

I_FILE=$1
O_FILE=$2

BINARIES=(pngoptimizercl pngout optipng leanify pngwolf pngrewrite advpng ect)
EXIT_STATUS=0

echo_error () {
  echo "ERROR: $1" 2>&1;
}

###
# Check existence of binaries
###

if [[ ! -d 'bin' ]]; then
  echo_error "Binary directory not found"
  EXIT_STATUS=1;

else
  for BINARY in "${BINARIES[@]}"; do
    if [[ ! -x "bin/$BINARY" ]]; then
      echo_error "$BINARY binary wasn't found or isn't executable"
      EXIT_STATUS=1
    fi
  done
fi

if [[ $EXIT_STATUS -gt 0 ]]; then exit 1; fi

###
# Handle number of arguments, existence, and permissions of arguments
###

if [[ $# -lt 1 || $# -gt 2 ]]; then
  echo_error "Wrong number of arguments"
  exit 1
fi

if [[ ! -a $I_FILE ]]; then
  echo_error "Input file or directory doesn't exist"
  exit 1
fi

if [[ $# -eq 1 ]]; then

  if [[ -d $I_FILE ]]; then
    if [[ !( -r $I_FILE && -w $I_FILE && -x $I_FILE ) ]]; then
      echo_error "Input/output directory must be readable, writable, and executable"
      exit 1
    fi
  else
    if [[ !( -r $I_FILE && -w $I_FILE ) ]]; then
      echo_error "Input/output file must be readable and writable"
      exit 1
    fi
  fi

fi

if [[ $# -eq 2 ]]; then

  # Arguments must be different for read/write detection to work
  if [[ $I_FILE == $O_FILE ]]; then
    echo_error "Arguments must not repeat"
    exit 1
  fi

  # A directory of images can't be saved to a single file
  if [[ -d $I_FILE && -f $O_FILE ]]; then
    echo_error "Directory can't be saved to a single file"
    exit 1
  fi

  # Handle input permissions
  if [[ -d $I_FILE ]]; then
    if [[ !( -r $I_FILE && -x $I_FILE ) ]]; then
      echo_error "Input directory must be readable and executable"
      exit 1
    fi
  else
    if [[ !( -r $I_FILE ) ]]; then
      echo_error "Input file must be readable"
      exit 1
    fi
  fi

  # Handle output permissions
  if [[ -d $O_FILE ]]; then
    if [[ !( -w $O_FILE && -x $O_FILE ) ]]; then
      echo_error "Output directory must be writable and executable"
      exit 1
    fi
  else
    if [[ !( -w $O_FILE ) ]]; then
      echo_error "Output file must be writable"
      exit 1
    fi
  fi

fi

# Usage: compress input [output]
# compress file.png -> correct, input must be read and write
# compress file.png file.png -> correct, input must be read, output must be write
# compress file.png directory -> correct, input must be read, output must be write and execute
#
# compress directory -> correct, input must be read, write, and execute
# compress directory directory -> correct, input must be read and execute, output must be write and execute
# compress directory file -> incorrect
#
# Check for null strings and empty strings
# Update to run on multiple threads

###
# Handle enumeration of input, error when needed for recursive directories
###

if [[ $# -eq 1 ]]; then
  if [[ -d $I_FILE ]]; then
    # Enumerate directory!
    # Compress file in-place!
  else
    # Compress file!
  fi
fi

if [[ $# -eq 2 ]]; then
  if [[ -d $I_FILE ]]; then
    # Enumerate directory!
    # Compress to directory!
  else
    # Compress to output (file or directory)!
    if [[ -d $O_FILE ]]; then
      # Output by appending original filename to output directory path!
    else
      # Compress and output to file!
    fi
  fi
fi
