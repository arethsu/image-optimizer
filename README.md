# Optimize PNG

This tool will utilize multiple image optimizers to achieve the best possible results in terms of compression, without any quality loss.

* PngOptimizer 154 919 -> 104 226 https://github.com/hadrien-psydk/pngoptimizer
* TruePNG 104 226 -> 101 415 xx
* PNGOut http://advsys.net/ken/utils.htm
* OptiPNG http://optipng.sourceforge.net/
* Leanify 101 415 -> 94 963 https://github.com/JayXon/Leanify
* pngwolf 94 963 -> 94 864 https://github.com/jibsen/pngwolf-zopfli
* pngrewrite
* advpng https://github.com/amadvance/advancecomp
* ECT 94 864 -> 94 695 https://github.com/fhanau/Efficient-Compression-Tool
* pingo xx
* deflopt 94 695 -> 94 694 xx
* defluff xx
* deflopt xx
